import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/Home.vue'
import AboutView from '../views/About.vue'
import ContactVue from '../views/Contact.vue'
import Products from '../views/Products.vue'
import Detalle from '../views/Detalle.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView
    },
    {
      path: '/contact',
      name: 'contact',
      component: ContactVue
    },
    {
      path: '/products',
      name: 'products',
      component: Products
    },
    {
      path: '/detalle/:id',
      name: 'detalle',
      component: Detalle
    }
  ]
})

export default router
